import java.util.Arrays;

public class WordsToChoose {
    private String[] words = {"table", "awkward", "zombie", "yacht", "insistence", "opponent"};
    private String randomWord = "";
    private String hiddenWord = "";
    private int guessedChar = 0;
    private int numberOfCharactersInRandomWord = 0;
    private int wrongAnswer = 0;


    public void getARandomWord() {
        int randomNumber = (int) (Math.random() * words.length);
        this.randomWord = words[randomNumber].replaceAll("", " ");
        this.randomWord = randomWord.substring(1, randomWord.length());
        numberOfCharactersInRandomWord = (int) Math.ceil(randomWord.length() / 2);
    }

    public void hideTheWord() {
        hiddenWord = this.randomWord.replaceAll("[a-zA-Z]", "_");
        this.hiddenWord = hiddenWord.substring(0, hiddenWord.length() - 1);
    }

    public void replaceEmptySpace(String userInput) {
        char[] character = userInput.toCharArray();
        char[] chars = stringToChar();
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == character[0]) {
                hiddenWord = hiddenWord.substring(0, i) + chars[i] + hiddenWord.substring(i + 1);
                guessedChar++;
            }
        }
    }

    public boolean hasWin() {
        if (guessedChar == numberOfCharactersInRandomWord) {
            System.out.println("Congratiulations! You guessed " + randomWord.replaceAll(" ", ""));
            return true;
        } else {
            return false;
        }
    }

    public char[] stringToChar() {
        return this.randomWord.toCharArray();
    }

    public String[] getWords() {
        return words;
    }

    public String getRandomWord() {
        return randomWord;
    }

    public String getHiddenWord() {
        return hiddenWord;
    }

    public int getGuessedChar() {
        return guessedChar;
    }

    public int getNumberOfCharactersInRandomWord() {
        return numberOfCharactersInRandomWord;
    }

    public int getWrongAnswer() {
        return wrongAnswer;
    }
}
