import java.util.Scanner;

public class TheHangman {
    private Scanner input = new Scanner(System.in);
    private WordsToChoose words;
    private boolean hasWin;
    private Validator validator;
    public int numberOfGuesses;
    public int wrongGuesses = 0;

    public TheHangman() {
        this.hasWin = false;
        this.validator = new Validator();
        this.words = new WordsToChoose();
    }

    public void start() {
        words.getARandomWord();
        words.hideTheWord();


        while (!hasWin) {
            System.out.println(words.getRandomWord());
            System.out.println(words.getHiddenWord());
            System.out.println("What's your guess!");
            String userInput = input.nextLine();
            boolean isValidLetter = validator.isAValidLetter(userInput);

            if (isValidLetter) {
                if (validator.isLetterInRandomWord(words.stringToChar(), userInput)) {
                    words.replaceEmptySpace(userInput);
                }else {
                    wrongGuesses++;
                }
            }
            //System.out.println("wrong guesses = " + wrongGuesses);
            if (words.hasWin()) {
                hasWin = true;
                System.out.println("You had "  + wrongGuesses + " wrong guesses!");
            }
        }
    }

    public WordsToChoose getWords() {
        return words;
    }

    public boolean isHasWin() {
        return hasWin;
    }

    public Validator getValidator() {
        return validator;
    }
}
