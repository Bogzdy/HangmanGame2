import java.util.ArrayList;


public class Validator {
    ArrayList<Character> usedChar = new ArrayList<Character>();
    public int guesses = 0;

    public boolean isLetterInRandomWord(char[] chars, String userInput){
        boolean bool = false;
        char[] convertedChar = userInput.toCharArray();
        for (int i = 0; i < chars.length; i++){
            if (convertedChar[0] == chars[i]){
                bool = true;
            }
        }
        return bool;
    }

    public boolean isAValidLetter(String userInput) {
        if (this.isOneChar(userInput) && this.hasNotBeenUsed(userInput)) {
            char[] convertedChar = userInput.toCharArray();
            usedChar.add(convertedChar[0]);
            guesses ++;
            return true;
        } else {
            return false;
        }
    }

    private boolean isOneChar(String userInput) {
        if (userInput.length() == 1) {
            return true;
        } else {
            System.out.println("Only one character!");
            return false;
        }
    }

    private boolean hasNotBeenUsed(String userInput) {
        char[] convertedChar = userInput.toCharArray();
        if (usedChar.contains(convertedChar[0])) {
            System.out.println("You already used " + convertedChar[0]);
            return false;
        } else {
            return true;
        }
    }
}
